﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace HabboAssetsDownloader
{
    class Program
    {
        private static Config Configuration;
        private static List<string> Blacklist = new List<string>();       

        internal static void Main(string[] args)
        {            
            Configuration = new Config("config.conf");

            Blacklist.Add("floortile.swf");
            Blacklist.Add("soft_jaggara_norja.swf");
            Blacklist.Add("ticket.swf");
            Blacklist.Add("kinkysofa.swf");
            Blacklist.Add("horse_hairdye_04_horsesho.swf");
            Blacklist.Add("post.it.swf");
            Blacklist.Add("post.it.vd.swf");
            Blacklist.Add("chess.swf");
            Blacklist.Add("tictactoe.swf");
            Blacklist.Add("battleship.swf");
            Blacklist.Add("poker.swf");
            Blacklist.Add("poster.swf");
            Blacklist.Add("ads_clwall3.swf");

            Console.WriteLine("SimpleFurniDownloader - Based on AKIIX Code");
            Console.WriteLine("Standalone version By Bjork! Enjoy!");           
            DownloadFurnis();
            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("Finished my job! Have a Good Day! You can close the window");
            Console.ReadKey();
        }

        internal static void DownloadFurnis()
        {
            string Key = string.Empty;
            string Hotel = GetConfig().data["Hotel"];

            bool DumpFurni = Convert.ToBoolean(GetConfig().data["DumpFurni"]);
            bool DumpIcons = Convert.ToBoolean(GetConfig().data["DumpIcons"]);

            bool DumpCatalogueIcons = Convert.ToBoolean(GetConfig().data["DumpCatalogueIcons"]);
            int CatalogueIconMax = Convert.ToInt32(GetConfig().data["CatalogueIconMax"]);

            bool DumpSounds = Convert.ToBoolean(GetConfig().data["DumpSounds"]);
            int SoundsMax = 800;

            bool DumpClothes = Convert.ToBoolean(GetConfig().data["DumpClothes"]);

            string AllFolder = "all";

            bool UseProxy = Convert.ToBoolean(GetConfig().data["UseProxy"]);
            string ProxyIp = GetConfig().data["ProxyIP"];
            int ProxyPort = Convert.ToInt32(GetConfig().data["ProxyPort"]);

            bool DumpBadges = Convert.ToBoolean(GetConfig().data["DumpBadges"]);

            if (DumpFurni)
                Console.WriteLine("The App will download furnis.");
            else
                Console.WriteLine("The App will not download furnis.");

            if (DumpIcons)
                Console.WriteLine("The App will download furni icons.");
            else
                Console.WriteLine("The App will not download furni icons.");

            if (DumpCatalogueIcons)
                Console.WriteLine("The App will download catalogue icons.");
            else
                Console.WriteLine("The App will not download catalogue icons.");

            if (DumpSounds)
                Console.WriteLine("The App will download sounds.");
            else
                Console.WriteLine("The App will not download sounds.");

            if (DumpClothes)
                Console.WriteLine("The App will download clothes.");
            else
                Console.WriteLine("The App will not download clothes.");

            if (DumpBadges)
            {
                Console.WriteLine("The App will download badges in " + Hotel + " Hotel");
            }
            else
                Console.WriteLine("The App will not download badges.");

            string fixedUrl = @"http://images.habbo.com/dcr/hof_furni/";
            string fixedUrlMP3 = @"http://images.habbo.com/dcr/hof_furni/mp3/";
            string CimagesUrl = @"https://images.habbo.com/c_images/";
            string GordonUrl = @"https://images.habbo.com/gordon/";
            string downloadUrl = "";
            string iconUrl = "";
            string dynamicUrl = "";
            string dynamicIconUrl = "";
            string furniName = "";
            string revision = "";
            string clothesName = "";
            string Production = "";            

            WebClient VariablesDownloader = new WebClient();
            WebClient TextsDownloader = new WebClient();
            WebClient FurnidataDownloader = new WebClient();
            WebClient FiguremapDownloader = new WebClient();

            WebProxy GlobalProxy = null;

            if (UseProxy)
            {
                Console.WriteLine("");
                Console.WriteLine("Proxying: " + ProxyIp + ":" + ProxyPort);
                GlobalProxy = new WebProxy(ProxyIp, ProxyPort);
                Thread.Sleep(3000);
                Console.WriteLine("");
            }

            Console.WriteLine("Downloading All Externals...");
            Console.WriteLine("");

            try
            {
                if (!Directory.Exists(@"external/")) Directory.CreateDirectory(@"external/");
                if (!Directory.Exists(@"XML/")) Directory.CreateDirectory(@"XML/");

               #region External Variables
                Key = "external_variables";

                if (UseProxy)
                    VariablesDownloader.Proxy = GlobalProxy;

                VariablesDownloader.Encoding = System.Text.Encoding.UTF8;
                VariablesDownloader.Headers.Add("User-Agent", "Mozilla/5.0 (Linux; U; Android 4.0.3; ko-kr; LG-L160L Build/IML74K) AppleWebkit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30");

                VariablesDownloader.DownloadFile(new Uri("http://www.habbo." + Hotel + "/gamedata/external_variables/ea494cd29b3693af2eef4939a606bd4cba30c24b"), @"external/external_variables_" + Hotel + ".txt");
                Console.WriteLine("Downloaded External Variables TXT from Habbo." + Hotel);                
                #endregion

                #region External Texts
                Key = "external_texts";

                if (UseProxy)
                    TextsDownloader.Proxy = GlobalProxy;

                TextsDownloader.Encoding = System.Text.Encoding.UTF8;
                TextsDownloader.Headers.Add("User-Agent", "Mozilla/5.0 (Linux; U; Android 4.0.3; ko-kr; LG-L160L Build/IML74K) AppleWebkit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30");

                TextsDownloader.DownloadFile(new Uri("https://www.habbo." + Hotel + "/gamedata/external_flash_texts/ea494cd29b3693af2eef4939a606bd4cba30c24b"), @"external/external_flash_texts_" + Hotel + ".txt");
                Console.WriteLine("Downloaded External Texts from Habbo." + Hotel);
                #endregion

                #region Furnidata
                Key = "furnidata";

                if (UseProxy)
                    FurnidataDownloader.Proxy = GlobalProxy;

                FurnidataDownloader.Encoding = System.Text.Encoding.UTF8;
                FurnidataDownloader.Headers.Add("User-Agent", "Mozilla/5.0 (Linux; U; Android 4.0.3; ko-kr; LG-L160L Build/IML74K) AppleWebkit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30");

                FurnidataDownloader.DownloadFile(new Uri("http://www.habbo." + Hotel + "/gamedata/furnidata_xml/ea494cd29b3693af2eef4939a606bd4cba30c24b"), @"XML/furnidata_xml_" + Hotel + ".xml");
                Console.WriteLine("Downloaded Furnidata XML from Habbo." + Hotel);
                #endregion

                #region Figuremap
                Key = "figuremap";

                StreamReader reader = File.OpenText(@"external/external_variables_" + Hotel + ".txt");

                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    string[] items = line.Split('\t');
                    foreach (string Prod in items)
                    {
                        if (Prod.StartsWith("flash.client.url="))
                        {
                            Production = Prod.Substring(43).TrimEnd('/');                            
                        }
                    }
                }

                if (UseProxy)
                    FiguremapDownloader.Proxy = GlobalProxy;

                FiguremapDownloader.Encoding = System.Text.Encoding.UTF8;
                FiguremapDownloader.Headers.Add("User-Agent", "Mozilla/5.0 (Linux; U; Android 4.0.3; ko-kr; LG-L160L Build/IML74K) AppleWebkit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30");

                if (!Directory.Exists(@"XML/" + Production + "/")) Directory.CreateDirectory(@"XML/" + Production + "/");

                FiguremapDownloader.DownloadFile(new Uri(GordonUrl + "/" + Production + "/figuremap.xml"), @"XML/" + Production + "/figuremap.xml");
                Console.WriteLine("Downloaded Figuremap XML from Habbo." + Hotel.ToUpper() + "");                
                #endregion

                VariablesDownloader.Dispose();
                TextsDownloader.Dispose();
                FurnidataDownloader.Dispose();
                FiguremapDownloader.Dispose();

            }
            catch (Exception e)
            {
                Console.WriteLine("Error with downloading the " + Key + " : " + e.ToString());
                Console.ReadKey();
            }

            Console.WriteLine("");
            Console.WriteLine("Current PRODUCTION: " + Production);
            Console.WriteLine("");
            Thread.Sleep(5000);

            Console.WriteLine("Downloading assets...");
            Console.WriteLine("");

            WebClient furniDownloader = new WebClient();
            furniDownloader.Encoding = System.Text.Encoding.UTF8;
            furniDownloader.Headers.Add("User-Agent", "Mozilla/5.0 (Linux; U; Android 4.0.3; ko-kr; LG-L160L Build/IML74K) AppleWebkit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30");

            if (DumpFurni || DumpIcons)
            {
                Console.WriteLine("DUMPING FURNIS &/OR ICONS...");
                Console.WriteLine("");

                XDocument xDoc = XDocument.Load(@"XML/furnidata_xml_" + Hotel + ".xml");

                var downloadRoomList = xDoc.Descendants("roomitemtypes").Descendants("furnitype");
                var downloadWallList = xDoc.Descendants("wallitemtypes").Descendants("furnitype");
                foreach (var downloadRoomItem in downloadRoomList)
                {
                    try
                    {
                        furniName = downloadRoomItem
                            .Attribute("classname")
                            .Value;
                        revision = downloadRoomItem
                            .Element("revision")
                            .Value;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.ToString());
                    }

                    try
                    {
                        if (DumpFurni)
                        {
                            string[] split = furniName.Split('*');
                            downloadUrl = split[0] + ".swf";
                            dynamicUrl = fixedUrl + revision + "/" + downloadUrl;

                            if (!Directory.Exists(@"hof_furni/" + revision + @"\")) Directory.CreateDirectory(@"hof_furni/" + revision + @"\");
                            if (!Directory.Exists(@"hof_furni/" + AllFolder + @"\")) Directory.CreateDirectory(@"hof_furni/" + AllFolder + @"\");

                            if (!File.Exists(@"hof_furni/" + revision + @"\" + downloadUrl))
                                Log("Downloaded FLOOR SWF: " + downloadUrl);

                            furniDownloader.DownloadFile(new Uri(dynamicUrl), @"hof_furni/" + revision + @"\" + downloadUrl);
                            File.Copy(@"hof_furni/" + revision + @"\" + downloadUrl, @"hof_furni/" + AllFolder + @"\" + downloadUrl);

                            Console.WriteLine(dynamicUrl + " - 100%");
                        }

                        if (DumpIcons)
                        {
                            string replace = furniName.Replace("*", "_");
                            iconUrl = replace + "_icon.png";
                            dynamicIconUrl = fixedUrl + revision + "/" + iconUrl;

                            if (!Directory.Exists(@"icons/")) Directory.CreateDirectory(@"icons/");

                            if (!File.Exists(@"icons/" + iconUrl))
                                Log("Downloaded FLOOR ICON: " + iconUrl);

                            furniDownloader.DownloadFile(new Uri(dynamicIconUrl), @"icons/" + iconUrl);
                            Console.WriteLine(dynamicIconUrl + " - 100%");
                        }
                    }
                    catch { }
                }

                foreach (var downloadWallItem in downloadWallList)
                {
                    furniName = downloadWallItem
                    .Attribute("classname")
                    .Value;
                    revision = downloadWallItem
                        .Element("revision")
                        .Value;

                    try
                    {
                        if (DumpFurni)
                        {
                            string[] split = furniName.Split('*');
                            downloadUrl = split[0] + ".swf";
                            dynamicUrl = fixedUrl + revision + "/" + downloadUrl;

                            if (!Directory.Exists(@"hof_furni/" + revision + @"\")) Directory.CreateDirectory(@"hof_furni/" + revision + @"\");
                            if (!Directory.Exists(@"hof_furni/" + AllFolder + @"\")) Directory.CreateDirectory(@"hof_furni/" + AllFolder + @"\");

                            if (!File.Exists(@"hof_furni/" + revision + @"\" + downloadUrl))
                                Log("Downloaded WALL SWF: " + downloadUrl);

                            furniDownloader.DownloadFile(new Uri(dynamicUrl), @"hof_furni/" + revision + @"\" + downloadUrl);
                            File.Copy(@"hof_furni/" + revision + @"\" + downloadUrl, @"hof_furni/" + AllFolder + @"\" + downloadUrl); 
                            Console.WriteLine(dynamicUrl + " - 100%");
                        }

                        if (DumpIcons)
                        {
                            string replace = furniName.Replace("*", "_");
                            iconUrl = replace + "_icon.png";
                            dynamicIconUrl = fixedUrl + revision + "/" + iconUrl;
                            
                            if (!Directory.Exists(@"icons/")) Directory.CreateDirectory(@"icons/");

                            if (!File.Exists(@"icons/" + iconUrl))
                                Log("Downloaded WALL ICON: " + iconUrl);

                            furniDownloader.DownloadFile(new Uri(dynamicIconUrl), @"icons/" + iconUrl);
                            Console.WriteLine(dynamicIconUrl + " - 100%");
                        }
                    }
                    catch { }
                }
            }

            Console.WriteLine("");

            if (DumpClothes)
            {
                Console.WriteLine("DUMPING CLOTHES...");
                Console.WriteLine("");

                XDocument xDoc = XDocument.Load(@"XML/" + Production + "/figuremap.xml");

                var clothes = xDoc.Descendants("map").Descendants("lib");
                foreach (var downloadClothes in clothes)
                {
                    try
                    {
                        clothesName = downloadClothes
                            .Attribute("id")
                            .Value;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.ToString());
                    }

                    try
                    {
                        downloadUrl = clothesName + ".swf";
                        dynamicUrl = GordonUrl + Production + "/" + downloadUrl;

                        if (!Directory.Exists(@"clothes/"))
                            Directory.CreateDirectory(@"clothes/");

                        if (!File.Exists(@"clothes/" + downloadUrl))
                        {
                            furniDownloader.DownloadFile(new Uri(dynamicUrl), @"clothes/" + downloadUrl);
                            Console.WriteLine(dynamicUrl + " - 100%");
                            Log("Downloaded CLOTHES: " + downloadUrl);
                        }                        
                    }
                    catch { }
                }
            }

            Console.WriteLine("");

            if (DumpCatalogueIcons)
            {
                Console.WriteLine("DUMPING CATALOG ICONS UP TO " + CatalogueIconMax + "...");
                Console.WriteLine("");

                if (!Directory.Exists(@"catalogue/")) Directory.CreateDirectory(@"catalogue/");

                int tries = 0;
                for (int i = 1; i <= CatalogueIconMax; i++)
                {
                    if (tries > 5)
                        CatalogueIconMax = 0;                    
                    try
                    {
                        if (!File.Exists(@"catalogue/icon_" + i + ".png"))
                            Log("Downloaded CATALOG ICON: icon_" + i + ".png");

                        furniDownloader.DownloadFile(new Uri(CimagesUrl + "catalogue/icon_" + i + ".png"), @"catalogue/icon_" + i + ".png");
                        Console.WriteLine(CimagesUrl + "catalogue/" + i + ".png - 100%");
                    }
                    catch (Exception e)
                    {
                        if (i > 200)
                            tries++;

                        //Console.WriteLine("icon_" + i + ".png doesn't exists... / [TRY " + tries + "]");
                        Log("CATALOG ICON DOESN'T EXISTS: icon_" + i + ".png");
                    }
                }
            }

            Console.WriteLine("");

            if (DumpBadges)
            {
                Console.WriteLine("DUMPING BADGES...");
                Console.WriteLine("");

                if (!Directory.Exists(@"album1584\")) Directory.CreateDirectory(@"album1584\");
                if (!Directory.Exists(@"SQLS\")) Directory.CreateDirectory(@"SQLS\");

                StringBuilder BadgesSB = new StringBuilder();
                using (StreamReader BadgesSR = new StreamReader(@"external/external_flash_texts_" + Hotel.ToLower() + ".txt"))
                {
                    string badge;
                    while ((badge = BadgesSR.ReadLine()) != null)
                    {
                        BadgesSB.AppendLine(badge);
                    }
                }

                string Badges = BadgesSB.ToString();

                using (StringReader BadgeSR = new StringReader(Badges))
                {
                    string txtbadge;
                    while ((txtbadge = BadgeSR.ReadLine()) != null)
                    {
                        if (txtbadge.StartsWith("badge_name_"))
                        {
                            string badge = txtbadge.Replace("badge_name_", "").Split('=')[0].Replace(" ", "");
                            string Uri = "http://habboo-a.akamaihd.net/c_images/album1584/" + badge + ".gif";
                            try
                            {
                                string ADM = string.Empty;

                                if (badge.Contains("ADM"))
                                {
                                    ADM = "mod_tool";
                                }

                                if (!File.Exists(@"album1584\" + badge + ".gif"))
                                {
                                    furniDownloader.DownloadFile(new Uri(Uri), @"album1584\" + badge + ".gif");
                                    FileStream errWriter = new System.IO.FileStream(@"SQLS\badge_definitions.sql", System.IO.FileMode.Append, System.IO.FileAccess.Write);
                                    byte[] Msg = System.Text.Encoding.Default.GetBytes(Environment.NewLine + "REPLACE INTO `badge_definitions` VALUES ('" + badge + "', '" + ADM + "');");
                                    errWriter.Write(Msg, 0, Msg.Length);
                                    errWriter.Dispose();

                                    Console.WriteLine("Downloaded Badge: " + badge + " - 100%");
                                    Log("Downloaded BADGE:  " + badge + ".gif");
                                }
                            }
                            catch { }
                        }
                    }
                }

                Console.WriteLine("");

                if (DumpSounds)
                {
                    Console.WriteLine("DUMPING SOUNDS UP TO " + SoundsMax + "...");
                    Console.WriteLine("");

                    if (!Directory.Exists(@"mp3/")) Directory.CreateDirectory(@"mp3/");

                    int tries = 0;
                    for (int i = 1; i <= SoundsMax; i++)
                    {
                        if (tries > 5)
                            SoundsMax = 0;
                        
                        try
                        {
                            if (!File.Exists(@"mp3/sound_machine_sample_" + i + ".mp3"))
                                Log("Downloaded SOUND:  sound_machine_sample_" + i + ".mp3");

                            furniDownloader.DownloadFile(new Uri(fixedUrlMP3 + "sound_machine_sample_" + i + ".mp3"), @"mp3/sound_machine_sample_" + i + ".mp3");
                            Console.WriteLine(fixedUrlMP3 + "sound_machine_sample_" + i + ".mp3 - 100%");
                        }
                        catch (Exception e)
                        {
                            if (i > 700)
                                tries++;

                            //Console.WriteLine(fixedUrlMP3 + "sound_machine_sample_" + i + ".mp3 doesn't exists... / [TRY " + tries + "]");
                            Log("SOUND DOESN'T EXISTS: sound_machine_sample_" + i + ".mp3");
                        }
                    }
                }

                furniDownloader.Dispose();
            }
        }

        public static void Log(string logText)
        {
            foreach (string key in Blacklist)
                if (logText.ToLower().Contains(key))
                    return;

            string filename = "-" + DateTime.Now.ToString("dd-MM-yy");
            WriteToFile(@"Logs\logs" + filename + ".txt", logText + "\r\n");
        }

        private static void WriteToFile(string Path, string content)
        {
            try
            {
                if (!Directory.Exists(@"Logs/")) Directory.CreateDirectory(@"Logs/");
                FileStream Writer = new FileStream(Path, FileMode.Append, FileAccess.Write);
                byte[] Msg = Encoding.ASCII.GetBytes(Environment.NewLine + "[" + DateTime.Now.ToString("dd/MM/yyyy | HH:mm:ss") + "] " + content);
                Writer.Write(Msg, 0, Msg.Length);
                Writer.Dispose();
            }
            catch (Exception e)
            {
                Console.WriteLine("Could not write to file: " + e + ":" + content);
            }
        }       

        public static Config GetConfig()
        {
            return Configuration;
        }
    }
}
