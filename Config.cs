﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HabboAssetsDownloader
{
    class Config
    {
        public Dictionary<string, string> data;

        public Config(string filePath)
        {
            data = new Dictionary<string, string>();

            if (!File.Exists(filePath))
            {
                throw new Exception("Unable to locate configuration file at '" + filePath + "'.");
            }

            try
            {
                using (StreamReader stream = new StreamReader(filePath))
                {
                    string line = null;

                    while ((line = stream.ReadLine()) != null)
                    {
                        if (line.Length < 1 || line.StartsWith("#"))
                        {
                            continue;
                        }

                        if (line.Contains("="))
                        {
                            String Key = line.Split('=')[0];
                            String Val = line.Split('=')[1];

                            data.Add(Key, Val);
                        }
                    }

                    stream.Dispose();
                    stream.Close();
                }
            }
            catch (Exception e)
            {
                throw new Exception("Could not process configuration file: " + e.Message);
            }
        }
    }
}